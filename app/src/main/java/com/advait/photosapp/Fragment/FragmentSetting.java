package com.advait.photosapp.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.advait.photosapp.R;
import com.advait.photosapp.Utils.SettingValue;
import com.suke.widget.SwitchButton;

public class FragmentSetting extends Fragment {

    SwitchButton switch_button,switch_button1;

    Context con;
    SettingValue settingValue;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_setting, container, false);

        con = getActivity();
        settingValue = SettingValue.getInstance(con);
        settingValue.loadSettingValue();

        switch_button = rootView.findViewById(R.id.switch_button);
        switch_button1 = rootView.findViewById(R.id.switch_button1);


        if(settingValue.isBlackWhite){
            switch_button.setChecked(true);
        }else {
            switch_button.setChecked(false);
        }

        if(settingValue.isFrame){
            switch_button1.setChecked(true);
        }else {
            switch_button1.setChecked(false);
        }

        switch_button.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                if(isChecked){
                    settingValue.saveSetting(settingValue.str_isBlackWhite, true);
                }else {
                    settingValue.saveSetting(settingValue.str_isBlackWhite, false);
                }
            }
        });

        switch_button1.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                if(isChecked){
                    settingValue.saveSetting(settingValue.str_isFrame, true);
                }else {
                    settingValue.saveSetting(settingValue.str_isFrame, false);
                }
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}