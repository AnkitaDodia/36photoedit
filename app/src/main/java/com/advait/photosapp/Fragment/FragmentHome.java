package com.advait.photosapp.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.advait.photosapp.Activity.NewBottomActivity;
import com.advait.photosapp.ExpandableGrid.Order;
import com.advait.photosapp.ExpandableGrid.OrderHistory;
import com.advait.photosapp.ExpandableGrid.Orderline;
import com.advait.photosapp.Models.CartData;
import com.advait.photosapp.Models.OrderEntry;
import com.advait.photosapp.R;
import com.advait.photosapp.Utils.Api;
import com.advait.photosapp.Utils.AppController;
import com.advait.photosapp.Utils.InternetStatus;
import com.advait.photosapp.Utils.SettingValue;
import com.advait.photosapp.Utils.UtilsFile;
import com.advait.photosapp.restinterface.RestInterface;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.sdsmdg.harjot.crollerTest.Croller;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

public class FragmentHome extends Fragment implements View.OnClickListener {

    Croller croller;
    RelativeLayout Layout;
    TextView lblUsername, lblCircle, lbltext1,txtFreeShipment,tvOpenCart;

    Context con;
    SettingValue settingValue;
    Date nextMemberShip;
    ProgressDialog dialog;
    boolean isInternet;

    ArrayList<Order> orderDataHistory = new ArrayList<>();
    ArrayList<Orderline> orderItemData = new ArrayList<>();
    ArrayList<OrderEntry> orderData = new ArrayList<>();
    int totalNumberOfImages = 0;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        con = getActivity();
        settingValue = SettingValue.getInstance(con);
        settingValue.loadSettingValue();

        isInternet = new InternetStatus().isInternetOn(con);
        dialog = new ProgressDialog(con);
        dialog.setMessage("Vent venligst..");
        dialog.setCancelable(false);

        croller = rootView.findViewById(R.id.croller);
        Layout = rootView.findViewById(R.id.Layout);
        lblUsername = rootView.findViewById(R.id.lblUsername);
        lblCircle = rootView.findViewById(R.id.lblCircle);
        lbltext1 = rootView.findViewById(R.id.lbltext1);
        tvOpenCart = rootView.findViewById(R.id.tvOpenCart);
        txtFreeShipment = rootView.findViewById(R.id.txtFreeShipment);
        croller.setMainCircleColor(Color.TRANSPARENT);
        croller.setBackCircleColor(Color.TRANSPARENT);
        croller.setLabel("");
        croller.setMax(36);
        croller.setStartOffset(0);
        croller.setProgress(settingValue.AvailablePhotos);
        croller.setIsContinuous(false);
        croller.setProgressPrimaryCircleSize(20);
        croller.setProgressSecondaryCircleSize(20);

        croller.setEnabled(false);

        Layout.setOnClickListener(this);
        tvOpenCart.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onResume() {

        if (isInternet) {
            LoginApi();
            sendCartListRequest();
            orderlistApi();
        }
        else {
            Toast.makeText(con, "Ingen internetforbindelse", Toast.LENGTH_LONG).show();
        }
        super.onResume();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnMenu:
                startActivity(new Intent(con, NewBottomActivity.class));
                break;
            case R.id.tvOpenCart:
                Fragment fragment = new FragmentCart();
                replaceFragment(fragment);
                break;
        }
    }

    public void setData(){

        Log.e("PHOTOS",""+settingValue.AvailablePhotos);
        lblUsername.setText(settingValue.name + "");
        totalNumberOfImages =  CalclulateTotalPhotos();
        lblCircle.setText(totalNumberOfImages + " ud "+settingValue.AvailablePhotos+" fotos");
        lbltext1.setText("Du har valgt " + totalNumberOfImages + " ud "+settingValue.AvailablePhotos+" fotos");

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            nextMemberShip = format.parse(settingValue.DateNextMembershipRenewal);
            Date currentDate= new Date();
            long diff = nextMemberShip.getTime() - currentDate.getTime();
            long seconds = diff / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;

            if(settingValue.FreeShipments == 0){

                if(diff > 0){
                    txtFreeShipment.setVisibility(View.VISIBLE);
                    txtFreeShipment.setText("Der er "+days+" dage til næste gratis fragt");
                }
            }
            //System.out.println(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void replaceFragment(Fragment someFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.MainFrame, someFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void LoginApi() {

        if (UtilsFile.hasConnection(con)) {

            String tag_json_obj = "json_obj_req";
//            dialog.show();
            String url = Api.baseurl + Api.login + SettingValue.Email + "&pw=" + SettingValue.Password;

            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,

                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("LOGIN", "==> " + response);
                            try {
                                JSONObject data = new JSONObject(response);
                                switch (data.getString("text")) {
                                    case "WrongPassword":
                                        Toast.makeText(con, "Adgangskoden er forkert, nulstil din adgangskode", Toast.LENGTH_SHORT).show();
                                        break;
                                    case "WrongEmail":
                                        Toast.makeText(getActivity(), "Emailen er ikke registreret i systemet, tjek venligst og prøv igen", Toast.LENGTH_SHORT).show();
                                        break;
                                    case "NoValidMembership":
                                        Toast.makeText(getActivity(), "Du har desværre ikke et gyldigt medlemskab", Toast.LENGTH_SHORT).show();
                                        break;
                                    case "ValidMembership":
                                        settingValue.saveSetting(settingValue.str_isLogin, true);
                                        settingValue.saveSetting(settingValue.str_name,data.getString("name"));
                                        settingValue.saveSetting(settingValue.str_UserId,data.getInt("UserId"));
                                        settingValue.saveSetting(settingValue.str_AvailablePhotos,data.getInt("AvailablePhotos"));
                                        settingValue.saveSetting(settingValue.str_FreeShipments,data.getInt("FreeShipments"));
                                        settingValue.saveSetting(settingValue.str_PriceExtraPhoto,data.getString("PriceExtraPhoto"));
                                        settingValue.saveSetting(settingValue.str_PriceExtraShipment,data.getString("PriceExtraShipment"));
                                        settingValue.saveSetting(settingValue.str_UrlPhotoRaw,data.getString("UrlPhotoRaw"));
                                        settingValue.saveSetting(settingValue.str_UrlPhotoDevelop,data.getString("UrlPhotoDevelop"));
                                        settingValue.saveSetting(settingValue.str_UrlPhotoThumb,data.getString("UrlPhotoThumb"));
                                        settingValue.saveSetting(settingValue.str_DateNextMembershipRenewal,data.getString("DateNextMembershipRenewal"));
                                        break;
                                }

                                setData();
                            } catch (JSONException e) {
                                if (dialog != null)
                                    dialog.dismiss();
                            } catch (Exception e) {
                            }

                            if (dialog != null)
                                dialog.dismiss();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (dialog != null)
                                dialog.dismiss();
                        }
                    }) {
            };

            AppController.getInstance().addToRequestQueue(stringRequest, tag_json_obj);
        }
    }

    private void sendCartListRequest()
    {
        try {
            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            Log.e("settingValue.UserId", "==> " + settingValue.UserId);

            String jsonStr = "{\"userid\":\""+String.valueOf(settingValue.UserId)+"\"}";

            TypedInput in = new TypedByteArray("application/json", jsonStr.getBytes("UTF-8"));

            restInterface.sendCartRequest(in, new Callback<CartData>() {
                @Override
                public void success(CartData model, retrofit.client.Response response) {

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("CART_RESPONSE", "" + new Gson().toJson(model));

                            switch (model.getText()) {
                                case "UnknownUser":
                                    break;
                                case "Du har ingen ordrer":
                                    break;
                                case "ValidOrdersEntry":

                                    orderData = model.getOrderEntries();
                                    totalNumberOfImages = orderData.size();
                                    setData();
                                    break;
                            }
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }
                @Override
                public void failure(RetrofitError error)
                {
                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void orderlistApi()
    {
        try {
            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            Log.e("settingValue.UserId", "==> " + settingValue.UserId);

            String jsonStr = "{\"userid\":\""+String.valueOf(settingValue.UserId)+"\"}";

            TypedInput in = new TypedByteArray("application/json", jsonStr.getBytes("UTF-8"));

            restInterface.orderHistoryRequest(in, new Callback<OrderHistory>() {
                @Override
                public void success(OrderHistory model, retrofit.client.Response response) {
                    if (response.getStatus() == 200) {
                        try {
                            Log.e("O_HITORY_RESPONSE---", "" + new Gson().toJson(model));

                            switch (model.getText()) {
                                case "UnknownUser":
//                                    Toast.makeText(con, "Ukendt bruger", Toast.LENGTH_SHORT).show();
                                    break;
                                case "Du har ingen ordrer":
//                                    Toast.makeText(con, "Ingen ordrer", Toast.LENGTH_SHORT).show();
                                    break;
                                case "NoOrderEntry":
//                                    txtNoOrder.setText("Du har ingen billeder i kurven");
//                                    txtNoOrder.setVisibility(View.VISIBLE);
//                                    list_order_history.setVisibility(View.GONE);
                                    break;
                                case "ValidOrders":
                                    orderDataHistory = model.getOrders();

//                                    Log.e("orderData",""+orderDataHistory.size());

                                    for (int i = 0; i < orderDataHistory.size(); i++)
                                    {
                                        orderItemData = new ArrayList<>();

                                        String date = orderDataHistory.get(i).getOrderDate();

                                        if(orderDataHistory.get(i).getOrderlines() != null)
                                        {
                                            orderItemData = orderDataHistory.get(i).getOrderlines();
//                                            Log.e("orderData","Size : "+orderItemData.size());
                                        }
                                    }
                                    setData();
//
//                                    Log.e("ORDER_ITEM_SIZE",""+orderItemData.size());
//                                    sectionedExpandableLayoutHelper.notifyDataSetChanged();
                                    break;
                            }
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }
                @Override
                public void failure(RetrofitError error)
                {
                    Log.e("ERROR", "" + error.getMessage());
                    Toast.makeText(con,"Noget gik galt. Prøv igen senere",Toast.LENGTH_LONG).show();
                }
            });
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public int CalclulateTotalPhotos(){

        int mNoOfImages = 0;
        int mNoOfOrderImage = 0;
        int totalNumberOfImage = 0;

//        Log.e("orderItemData.size()","size : "+orderItemData.size());

        for(int i = 0; i < orderData.size(); i++){

            mNoOfImages = mNoOfImages + Integer.parseInt(orderData.get(i).getQty());

//            Log.e("totalNumberOfImages","inside loop Qty Cart: "+mNoOfImages);
        }

        for(int i = 0; i < orderItemData.size(); i++)
        {
            mNoOfOrderImage = mNoOfOrderImage + Integer.parseInt(orderItemData.get(i).getQty());
//            Log.e("totalNumberOfImages","inside loop Qty History: "+mNoOfOrderImage);
        }

//        Log.e("totalNumberOfImagesCart","Qty : "+mNoOfImages);
//        Log.e("totalNumberOfImagesOrder","Qty : "+mNoOfOrderImage);

        totalNumberOfImage =  mNoOfImages + mNoOfOrderImage;

//        Log.e("totalNumberOfImages","Final Qty : "+totalNumberOfImage);

        return totalNumberOfImage;
    }
}