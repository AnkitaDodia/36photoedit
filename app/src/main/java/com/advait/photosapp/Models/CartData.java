package com.advait.photosapp.Models;

/**
 * Created by My 7 on 17-Jul-18.
 */

import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CartData {

    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("order_entries")
    @Expose
    private ArrayList<OrderEntry> orderEntries = null;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ArrayList<OrderEntry> getOrderEntries() {
        return orderEntries;
    }

    public void setOrderEntries(ArrayList<OrderEntry> orderEntries) {
        this.orderEntries = orderEntries;
    }

}