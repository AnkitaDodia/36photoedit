package com.advait.photosapp.restinterface;


import com.advait.photosapp.Models.CartData;
import com.advait.photosapp.Models.CreateOrder;
import com.advait.photosapp.ExpandableGrid.OrderHistory;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;
import retrofit.mime.TypedInput;

/**
 * Created by Adite-Ankita on 6/30/2016.
 */
public interface RestInterface
{
    public static String API_BASE_URL = "https://36fotos.dk/API/webservices";

    String CART_LIST = "/getbasketsnew.php";
    String CREATE_ORDER = "/createorder.php";
    String ADD_FCM = "/addfcm.php";
    String ORDER_HISTORY = "/getorderlist_new.php";

    //LogIn
    @POST(CART_LIST)
    public void sendCartRequest(
            @Body TypedInput typedInput,
            Callback<CartData> callBack);

    //CREATE_ORDER
    @FormUrlEncoded
    @POST(CREATE_ORDER)
    public void getCreateOrderRequest(@Field("UserId") String userId, Callback<CreateOrder> callback);

    //OrderHistory
    @POST(ORDER_HISTORY)
    public void orderHistoryRequest(@Body TypedInput typedInput,Callback<OrderHistory> callBack);
}
