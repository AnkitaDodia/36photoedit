package com.advait.photosapp.Utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Handler;

import java.util.ArrayList;

public class SettingValue {

    public static final String SettingsSharedName = "VIDEOSTATUS";
    public static SharedPreferences PrayersettingsPreferences;
    public static SharedPreferences.Editor PrayersettingsEditor;
    static Context userContext;

//    public static boolean isNewImage = true;
    public static String isEditFrom = "gallery";//history , cart

    public static Boolean isWelcome;
    public static String str_isWelcome = "isWelcome";

    public static Boolean isLogin;
    public static String str_isLogin = "isLogin";

    public static Boolean isBlackWhite;
    public static String str_isBlackWhite = "isBlackWhite";

    public static Boolean isFrame;
    public static String str_isFrame = "isFrame";

    public static String username;
    public static String str_username = "username";

    public static String name;
    public static String str_name = "name";

    public static int UserId;
    public static String str_UserId = "UserId";

    public static int AvailablePhotos;
    public static String str_AvailablePhotos = "AvailablePhotos";

    public static int FreeShipments;
    public static String str_FreeShipments = "FreeShipments";

    public static String PriceExtraPhoto;
    public static String str_PriceExtraPhoto = "PriceExtraPhoto";

    public static String PriceExtraShipment;
    public static String str_PriceExtraShipment = "PriceExtraShipment";

    public static String UrlPhotoRaw;
    public static String str_UrlPhotoRaw = "UrlPhotoRaw";

    public static String UrlPhotoDevelop;
    public static String str_UrlPhotoDevelop = "UrlPhotoDevelop";

    public static String UrlPhotoThumb;
    public static String str_UrlPhotoThumb = "UrlPhotoThumb";

    public static String DateNextMembershipRenewal;
    public static String str_DateNextMembershipRenewal = "DateNextMembershipRenewal";


    public static String Email;
    public static String str_Email = "Email";

    public static String Password;
    public static String str_Password = "Password";

//    public static String valRatio = "10 * 10 cm";

//    public static ArrayList<Bitmap> processedBitmapArray = new ArrayList<>();


    public void saveSetting(String key, String value) {
        PrayersettingsEditor.putString(key, value);
        PrayersettingsEditor.commit();
    }

    public void saveSetting(String key, Boolean value) {
        PrayersettingsEditor.putBoolean(key, value);
        PrayersettingsEditor.commit();
    }

    public void saveSetting(String key, int value) {
        PrayersettingsEditor.putInt(key, value);
        PrayersettingsEditor.commit();
    }

    public static String getSetting(String key, String defVal) {
        String val = PrayersettingsPreferences.getString(key, defVal);
        return val;
    }

    public boolean getSetting(String key, boolean defVal) {
        boolean val = PrayersettingsPreferences.getBoolean(key, defVal);
        return val;
    }

    public int getSetting(String key, int defVal) {
        int val = PrayersettingsPreferences.getInt(key, defVal);
        return val;
    }

    public void loadSettingValue() {

        isWelcome = getSetting(str_isWelcome, false);
        isLogin = getSetting(str_isLogin, false);

        isBlackWhite = getSetting(str_isBlackWhite, false);
        isFrame = getSetting(str_isFrame, false);

        name = getSetting(str_name, "");
        UserId = getSetting(str_UserId, 0);
        AvailablePhotos = getSetting(str_AvailablePhotos, 0);
        FreeShipments = getSetting(str_FreeShipments, 0);
        PriceExtraPhoto = getSetting(str_PriceExtraPhoto, "");
        PriceExtraShipment = getSetting(str_PriceExtraShipment, "");
        UrlPhotoDevelop = getSetting(str_UrlPhotoDevelop, "");
        UrlPhotoRaw = getSetting(str_UrlPhotoRaw, "");
        UrlPhotoThumb = getSetting(str_UrlPhotoThumb, "");
        DateNextMembershipRenewal = getSetting(str_DateNextMembershipRenewal, "");
        Email = getSetting(str_Email, "");
        Password = getSetting(str_Password, "");

    }

    static SettingValue instance;

    public static SettingValue getInstance(Context context) {
        if (instance == null) {
            userContext = context;
            PrayersettingsPreferences = userContext.getSharedPreferences(SettingsSharedName, 0);
            PrayersettingsEditor = PrayersettingsPreferences.edit();
            instance = new SettingValue();
        }
        return instance;
    }
}