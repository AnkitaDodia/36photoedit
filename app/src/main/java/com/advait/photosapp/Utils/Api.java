package com.advait.photosapp.Utils;

public class Api {

    public static String baseurl = "https://36fotos.dk/API/webservices/";

    public static String login = "login.php?email=";
    public static String resetpwd = "resetpassword.php?email=";
    public static String orderlist = "getorderlist.php?userid=";
    public static String delete = "removefrombasket.php?userid=";

    public static String addtobasket = "addtobasket.php";
}