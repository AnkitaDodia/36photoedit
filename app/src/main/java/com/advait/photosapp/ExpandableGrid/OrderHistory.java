package com.advait.photosapp.ExpandableGrid;

/**
 * Created by My 7 on 01-Aug-18.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class OrderHistory {

    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("orders")
    @Expose
    private ArrayList<Order> orders = null;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ArrayList<Order> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<Order> orders) {
        this.orders = orders;
    }
}