package com.advait.photosapp.ExpandableGrid;

/**
 * Created by lenovo on 2/23/2016.
 */
public interface ItemClickListener {
    void itemClicked(Orderline item);
    void itemClicked(Order section);
}
