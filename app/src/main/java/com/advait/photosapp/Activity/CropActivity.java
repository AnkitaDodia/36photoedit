package com.advait.photosapp.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.advait.photosapp.R;
import com.advait.photosapp.common.BaseActivity;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CropActivity extends BaseActivity {

    public final static String TAG = "CropActivity";
    public final static String APP_PATH_SD_CARD = "/36Photos/";

    Context mContext;
    CropImageView cropImageView;
    LinearLayout ll_crop_10_10, ll_crop_10_15, ll_crop_15_10, ll_crop_nulstil, ll_done;
    File image_file = null;
    boolean isFreeCrop = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop);

        mContext = this;

        cropImageView = findViewById(R.id.cropImageView);
        ll_crop_10_10  = findViewById(R.id.ll_crop_10_10);
        ll_crop_10_15  = findViewById(R.id.ll_crop_10_15);
        ll_crop_15_10  = findViewById(R.id.ll_crop_15_10);
        ll_crop_nulstil = findViewById(R.id.ll_crop_nulstil);
        ll_done  = findViewById(R.id.ll_done);

//        Intent intent = getIntent();
//        String id = intent.getStringExtra("ImageURI");

//        Uri myUri = Uri.parse(intent.getStringExtra("ImageURI"));

//        Log.e("ImageURI",""+myUri);
//        cropImageView.setImageUriAsync(myUri);
        cropImageView.setImageBitmap(OriginalBitmap);
        cropImageView.setFixedAspectRatio(true);
//        getDropboxIMGSize();
//        cropImageView.setMinCropResultSize(378,567);
//        cropImageView.setRequestedSize(150,50, CropImageView.RequestSizeOptions.RESIZE_EXACT);

//        Bitmap cropped = cropImageView.getCroppedImage();

        ll_crop_10_10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                cropImageView.setAspectRatio(1,1);

                ImageSize = "10 * 10 cm";
            }
        });

        ll_crop_10_15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                cropImageView.setAspectRatio(2,3);
                ImageSize = "10 * 15 cm";

            }
        });

        ll_crop_15_10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                cropImageView.setAspectRatio(3,2);
                ImageSize = "15 * 10 cm";
            }
        });

        ll_crop_nulstil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                cropImageView.setFixedAspectRatio(false);
//                isFreeCrop = true ;
//                ImageSize = "15 * 10 cm";
            }
        });

        ll_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                OriginalBitmap = cropImageView.getCroppedImage();

//                if(isFreeCrop){
//
//                    getDropboxIMGSize();
//                    isFreeCrop = false;
//
//                }

                Intent intent = getIntent();
//                intent.putExtra("key", value);
                setResult(RESULT_OK, intent);
                finish();
//                resize(cropped, 377, 566);
//                SizedBitmap(cropped);
            }
        });
    }

    private void getDropboxIMGSize() {
//        BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inJustDecodeBounds = true;
//        BitmapFactory.decodeFile(new File(uri.getPath()).getAbsolutePath(), options);

        int imageHeight = OriginalBitmap.getHeight();
        int imageWidth = OriginalBitmap.getWidth();


        Log.e("Actual size", "imageWidth: " + imageWidth + "  imageHeight : " + imageHeight);
        Log.e("Actual size", "imageWidth: " + dpToPx(imageWidth) + "  imageHeight : " + dpToPx(imageHeight));
        ImageSize = " " + dpToPx(imageWidth) + "  *  " + dpToPx(imageHeight) + " cm";

    }


    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px/100;
    }

//    private Bitmap scaleBitmap(Bitmap bm, int maxWidth, int maxHeight) {
//        int width = bm.getWidth();
//        int height = bm.getHeight();
//
//        Log.v("Pictures", "Width and height are " + width + "--" + height);
//
//        if (width > height) {
//            // landscape
//            float ratio = (float) width / maxWidth;
//            width = maxWidth;
//            height = (int)(height / ratio);
//        } else if (height > width) {
//            // portrait
//            float ratio = (float) height / maxHeight;
//            height = maxHeight;
//            width = (int)(width / ratio);
//        } else {
//            // square
//            height = maxHeight;
//            width = maxWidth;
//        }
//
//        Log.v("Pictures", "after scaling Width and height are " + width + "--" + height);
//
//        bm = Bitmap.createScaledBitmap(bm, width, height, true);
//        return bm;
//    }

//    private static Bitmap resize(Bitmap image, int maxWidth, int maxHeight) {
//        if (maxHeight > 0 && maxWidth > 0) {
//            int width = image.getWidth();
//            int height = image.getHeight();
//            float ratioBitmap = (float) width / (float) height;
//            float ratioMax = (float) maxWidth / (float) maxHeight;
//
//            int finalWidth = maxWidth;
//            int finalHeight = maxHeight;
//            if (ratioMax > 1) {
//                finalWidth = (int) ((float)maxHeight * ratioBitmap);
//            } else {
//                finalHeight = (int) ((float)maxWidth / ratioBitmap);
//            }
//            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
//            return image;
//        } else {
//            return image;
//        }
//    }

//    public Bitmap SizedBitmap(Bitmap aBitmap){
//
//        DisplayMetrics metrics = mContext.getResources().getDisplayMetrics();
//        float totalDIP_X = metrics.xdpi;
//        float totalDIP_Y = metrics.ydpi;
//
//        Bitmap Croped = Bitmap.createScaledBitmap (aBitmap, (int)(3.93701*totalDIP_X), (int)(3.93701*totalDIP_Y), false);
//
//        saveImageToExternalStorage(Croped);
//        return Croped;
//    }

  /*  public static Bitmap lessResolution (String filePath, int width, int height) {
        int reqHeight = height;
        int reqWidth = width;
        BitmapFactory.Options options = new BitmapFactory.Options();

        // First decode with inJustDecodeBounds=true to check dimensions
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(filePath, options);
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {

        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        return inSampleSize;
    }*/

    public boolean saveImageToExternalStorage(Bitmap image) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String currentDateandTime = sdf.format(new Date());

        String fullPath = Environment.getExternalStorageDirectory().getAbsolutePath() + APP_PATH_SD_CARD;
        Log.e(TAG, "fullPath  "+fullPath);
        try {
            File dir = new File(fullPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }

            OutputStream fOut = null;
            image_file = new File(fullPath, "IMG_" + currentDateandTime + ".png");
            image_file.createNewFile();
            fOut = new FileOutputStream(image_file);

            // 100 means no compression, the lower you go, the stronger the
            // compression
            image.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();

            galleryAddPic();

//            saved = true;
            return true;

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
//            Toast.makeText(ctx, "catch expection", Toast.LENGTH_SHORT).show();
//            saved = false;
            return false;
        }
    }

    private void galleryAddPic() {


        Log.e(TAG, "galleryAddPic");

        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
//	    File f = new File(image_file.getAbsolutePath());
        Uri contentUri = Uri.fromFile(image_file);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }
}
