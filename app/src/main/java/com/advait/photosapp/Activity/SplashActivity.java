package com.advait.photosapp.Activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.advait.photosapp.R;
import com.advait.photosapp.Utils.SettingValue;

public class SplashActivity extends AppCompatActivity {

    private static final int PERMISSION_CODE = 111;
    private String[] permission;

    Context con;
    SettingValue settingValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        con = this;
        settingValue = SettingValue.getInstance(con);
        settingValue.loadSettingValue();

        permission = new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_NETWORK_STATE,
                Manifest.permission.CAMERA
        };

        if (checkPermission()) {
            startHomeActivity();
        } else {
            ActivityCompat.requestPermissions(SplashActivity.this, permission, PERMISSION_CODE);
        }
    }

    private boolean checkPermission() {
        for (String aPermission : permission) {
            if (!(ContextCompat.checkSelfPermission(con, aPermission) == PackageManager.PERMISSION_GRANTED)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_CODE) {
            if (checkPermission()) {
                startHomeActivity();
            } else {
                ActivityCompat.requestPermissions(this, permission, PERMISSION_CODE);
            }
        }
    }

    private void startHomeActivity() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i;
                if (SettingValue.isLogin) {
                    i = new Intent(SplashActivity.this, NewBottomActivity.class);
                } else {
                    i = new Intent(SplashActivity.this, LoginActivity.class);
                }
                startActivity(i);
                finish();
            }
        }, 1000);
    }

    @Override
    public void onBackPressed() {
    }
}