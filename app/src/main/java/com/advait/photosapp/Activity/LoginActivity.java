package com.advait.photosapp.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.advait.photosapp.R;
import com.advait.photosapp.Utils.Api;
import com.advait.photosapp.Utils.AppController;
import com.advait.photosapp.Utils.InternetStatus;
import com.advait.photosapp.Utils.SettingValue;
import com.advait.photosapp.Utils.UtilsFile;
import com.advait.photosapp.common.BaseActivity;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    EditText txtUsername, txtPassword;
    TextView btnLogin,btnReset;

    Context con;
    SettingValue settingValue;
    ProgressDialog dialog;
    boolean isInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        con = this;
        settingValue = SettingValue.getInstance(con);
        settingValue.loadSettingValue();

        isInternet = new InternetStatus().isInternetOn(con);

        dialog = new ProgressDialog(con);
        dialog.setMessage("Vent venligst..");
        dialog.setCancelable(false);

        txtUsername = findViewById(R.id.txtUsername);
        txtPassword = findViewById(R.id.txtPassword);
        btnLogin = findViewById(R.id.btnLogin);
        btnReset = findViewById(R.id.btnReset);

        btnLogin.setOnClickListener(this);
        btnReset.setOnClickListener(this);

        txtUsername.setText("michael@frosterne.dk");//michael@frosterne.dk //abc@gmail.com
        txtPassword.setText("36fora80");
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(LoginActivity.this);
        myAlertDialog.setMessage("Er du sikker på at du vil lukke appen?");
        myAlertDialog.setTitle(getResources().getString(R.string.app_name));
        myAlertDialog.setIcon(R.drawable.logo);
        myAlertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface arg0, int arg1) {
                finish();
                System.exit(0);
            }
        });

        myAlertDialog.setNegativeButton("Fortryd", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
            }
        });

        myAlertDialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                if (txtUsername.getText().length() < 1)
                    txtUsername.setError("Indtast brugnavn");
                else if (txtPassword.getText().length() < 1)
                    txtPassword.setError("Indtast adgangskode");
                else
                if (isInternet) {
                    LoginApi();
                }
                else {
                    Toast.makeText(con, "Ingen internetforbindelse", Toast.LENGTH_LONG).show();
                }

//                startActivity(new Intent(con, NewBottomActivity.class));
//                finish();

                break;
            case R.id.btnReset:
                startActivity(new Intent(con, ResetPasswordActivity.class));
                finish();
                break;
        }
    }

    private void LoginApi() {

        if (UtilsFile.hasConnection(con)) {

            String tag_json_obj = "json_obj_req";

            dialog.show();

            String url = Api.baseurl + Api.login + txtUsername.getText().toString() + "&pw=" + txtPassword.getText().toString()
                    +"&fcm="+getFCMID()+"&device_id="+" ";//getIMEINumber()

            Log.e("URL", "==> " + url);

            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,

                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("LOGIN", "==> " + response);

                            try {
                                JSONObject data = new JSONObject(response);
                                switch (data.getString("text")) {
                                    case "WrongPassword":
                                        Toast.makeText(con, "Adgangskoden er forkert, nulstil din adgangskode", Toast.LENGTH_SHORT).show();
                                        break;
                                    case "WrongEmail":
                                        Toast.makeText(LoginActivity.this, "Emailen er ikke registreret i systemet, tjek venligst og prøv igen", Toast.LENGTH_SHORT).show();
                                        break;
                                    case "NoValidMembership":
                                        Toast.makeText(LoginActivity.this, "Du har desværre ikke et gyldigt medlemskab", Toast.LENGTH_SHORT).show();
                                        break;
                                    case "ValidMembership":
                                        settingValue.saveSetting(settingValue.str_isLogin, true);
                                        settingValue.saveSetting(settingValue.str_name,data.getString("name"));
                                        settingValue.saveSetting(settingValue.str_UserId,data.getInt("UserId"));
                                        settingValue.saveSetting(settingValue.str_AvailablePhotos,data.getInt("AvailablePhotos"));
                                        settingValue.saveSetting(settingValue.str_FreeShipments,data.getInt("FreeShipments"));
                                        settingValue.saveSetting(settingValue.str_PriceExtraPhoto,data.getString("PriceExtraPhoto"));
                                        settingValue.saveSetting(settingValue.str_PriceExtraShipment,data.getString("PriceExtraShipment"));
                                        settingValue.saveSetting(settingValue.str_UrlPhotoRaw,data.getString("UrlPhotoRaw"));
                                        settingValue.saveSetting(settingValue.str_UrlPhotoDevelop,data.getString("UrlPhotoDevelop"));
                                        settingValue.saveSetting(settingValue.str_UrlPhotoThumb,data.getString("UrlPhotoThumb"));
                                        settingValue.saveSetting(settingValue.str_DateNextMembershipRenewal,data.getString("DateNextMembershipRenewal"));
                                        settingValue.saveSetting(settingValue.str_Email,txtUsername.getText().toString());
                                        settingValue.saveSetting(settingValue.str_Password,txtPassword.getText().toString());
                                        startActivity(new Intent(con, NewBottomActivity.class));
                                        finish();
                                        break;
                                }
                            } catch (JSONException e) {
                                if (dialog != null)
                                    dialog.dismiss();
                                Toast.makeText(con,"Noget gik galt. Prøv igen senere",Toast.LENGTH_LONG).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(con,"Noget gik galt. Prøv igen senere",Toast.LENGTH_LONG).show();
                            }
                            if (dialog != null)
                                dialog.dismiss();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (dialog != null)
                                dialog.dismiss();
                            Toast.makeText(con,"Noget gik galt. Prøv igen senere",Toast.LENGTH_LONG).show();
                        }
                    }) {
            };

            AppController.getInstance().addToRequestQueue(stringRequest, tag_json_obj);
        }
    }
}