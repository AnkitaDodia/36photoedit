package com.advait.photosapp.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.widget.FrameLayout;
import android.provider.Settings.Secure;

import com.advait.photosapp.ExpandableGrid.Orderline;
import com.advait.photosapp.Models.OrderEntry;
import com.advait.photosapp.R;

/**
 * Created by My 7 on 17-Jul-18.
 */

public class BaseActivity extends AppCompatActivity
{
    FrameLayout MainFrame;
    public static boolean isFromEditing = false;
    public static OrderEntry orderEntry;
    public static Bitmap Croppedbitmap = null, OriginalBitmap = null;
    public static String ImageSize = "10 * 10 cm";

    public static Orderline mOrderlineHistory;//Orderline


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MainFrame = findViewById(R.id.MainFrame);
    }

    public void replaceFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.MainFrame, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public String getFCMID()
    {
        SharedPreferences sp = getSharedPreferences("FCM", MODE_PRIVATE);
        return sp.getString("FCM_ID","Null");
    }

//    public String getIMEINumber() {
//        TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//        String IMEINumber = mngr.getDeviceId();
//        return IMEINumber;
//
//        String android_id = Secure.getString(Context.getContentResolver(),
//                Secure.ANDROID_ID);
//    }
}
