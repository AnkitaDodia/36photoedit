package com.advait.photosapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.advait.photosapp.R;

import java.util.List;

public class DateAdapter extends RecyclerView.Adapter<DateAdapter.ViewHolder> {

    private Context ctx;
    List<String> imglist;
    int size;


    public DateAdapter(Context ctxx, List<String> list) {
        this.ctx = ctxx;
        this.imglist = list;
    }

    public DateAdapter(Context ctxx, int size) {
        this.ctx = ctxx;
        this.size = size;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_date, parent, false);
        return new DateAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
//        Glide.with(ctx).load(imglist.get(position)).into(holder.image);
    }

    @Override
    public int getItemCount() {
//        return imglist.size();
        return size;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {


        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}