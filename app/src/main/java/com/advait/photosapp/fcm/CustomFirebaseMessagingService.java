package com.advait.photosapp.fcm;

/**
 * Created by Adite-Ankita on 23-Aug-16.
 */
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.advait.photosapp.Activity.NewBottomActivity;
import com.advait.photosapp.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.io.IOException;

public class CustomFirebaseMessagingService extends FirebaseMessagingService
{
    private static final String TAG = CustomFirebaseMessagingService.class.getSimpleName();

    public static final int notifyID = 9001;

    String title,message,clickAction;

    @Override
    public void onCreate()
    {
        // TODO Auto-generated method stub
        super.onCreate();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage)
    {
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Message data payload: " + remoteMessage.getData());

            title = remoteMessage.getData().get("title");
            message = remoteMessage.getData().get("body");
            clickAction = remoteMessage.getData().get("click_action");

            Log.e("ANKITA_FROM_GET_DATA",""+title);
            Log.e("ANKITA_FROM_GET_DATA",""+message);
            Log.e("ANKITA_FROM_GET_DATA",""+clickAction);

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());

            title = remoteMessage.getNotification().getTitle();
            message = remoteMessage.getNotification().getBody();
            clickAction = remoteMessage.getNotification().getClickAction();

            Log.e("FROM_GET_NOTIFICATION",""+title);
            Log.e("FROM_GET_NOTIFICATION",""+message);
            Log.e("FROM_GET_NOTIFICATION",""+clickAction);
        }

        sendNotification(message,title,clickAction);
    }

    private void sendNotification(String msg,String title,String clickAction){
        //Creating a notification

        Intent resultIntent = new Intent(this, NewBottomActivity.class);
        resultIntent.putExtra("MSG", msg);
        resultIntent.putExtra("TITLE", title);
        resultIntent.putExtra("FROM", "SERVICE");
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0,resultIntent, PendingIntent.FLAG_ONE_SHOT);

        try
        {
            NotificationCompat.Builder mNotifyBuilder;
            NotificationManager mNotificationManager;

            mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            mNotifyBuilder = new NotificationCompat.Builder(this)
                    .setContentTitle(title)
                    .setContentText(msg)
//                    .setStyle(s)
                    .setSmallIcon(R.drawable.logo);

            // Set pending intent
            mNotifyBuilder.setContentIntent(resultPendingIntent);

            // Set Vibrate, Sound and Light
            int defaults = 0;
            defaults = defaults | Notification.DEFAULT_LIGHTS;
            defaults = defaults | Notification.DEFAULT_VIBRATE;
            defaults = defaults | Notification.DEFAULT_SOUND;

            mNotifyBuilder.setDefaults(defaults);
            // Set the content for Notification
            mNotifyBuilder.setContentText(msg);
            // Set autocancel
            mNotifyBuilder.setAutoCancel(true);
            // Post a notification
            mNotificationManager.notify(notifyID, mNotifyBuilder.build());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}