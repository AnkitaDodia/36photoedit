package com.advait.photosapp.fcm;

/**
 * Created by Adite-Ankita on 23-Aug-16.
 */

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.advait.photosapp.Adapter.CardAdapter;
import com.advait.photosapp.Models.CartData;
import com.advait.photosapp.Models.FcmResponse;
import com.advait.photosapp.Utils.SettingValue;
import com.advait.photosapp.common.BaseActivity;
import com.advait.photosapp.restinterface.RestInterface;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

public class CustomFirebaseInstanceIDService extends FirebaseInstanceIdService
{
    private static final String TAG = CustomFirebaseInstanceIDService.class.getSimpleName();


    String refreshedToken;

    @Override
    public void onTokenRefresh() {
        refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG, "Token_Value: " + refreshedToken);

        saveFCMID(refreshedToken);
    }

    public void saveFCMID(String id) {
        SharedPreferences sp = getSharedPreferences("FCM", MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putString("FCM_ID", id);
        spe.commit();
    }
}